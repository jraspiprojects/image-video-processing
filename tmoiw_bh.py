import cv2 
import numpy as np
import os
import sys
from time import sleep
from datetime import datetime

e = open(sys.argv[4] + ".csv", 'w+')
def get_match_confidence(img1, img2, mask=None):
    #if img1.shape != img2.shape:
    #    return False
    ## first try, using absdiffs
    # diff = cv2.absdiff(img1, img2)
    # h, w, d = diff.shape
    # total = h*w*d
    # num = (diff<20).sum()
    # print 'is_match', total, num
    # return num > total*0.90
    if mask is not None:
        img1 = img1.copy()
        img1[mask!=0] = 0
        img2 = img2.copy()
        img2[mask!=0] = 0
    ## using match
    match = cv2.matchTemplate(img1, img2, cv2.TM_CCOEFF_NORMED)
    _, confidence, _, _ = cv2.minMaxLoc(match)
    # print confidence
    return confidence

def error():
    words = "Um.... This is supposed to be some sort of error handling message. This shouldn't be happening. Please contact the Developer."
    for char in words:
        sleep(0.3)
        sys.stdout.write(char)
        sys.stdout.flush()

class checkImage():
    def __init__(self):
        self.LH = 0
        self.RH = 0
    def checkHandImage(self, hand):
        if hand == "LH":
            pic = sys.argv[2]
        elif hand == "RH":
            pic = sys.argv[3]
        else:
            error()
            sys.exit(1)
        img_rgb = cv2.imread(sys.argv[1])
        #print(img_rgb)
        # Convert it to grayscale 
        img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_BGR2GRAY) 
        #img_gray = img_rgb
        #print("image file read")
        # Read the template 
        template = cv2.imread(pic,0)
        #print("template read")

        img_rgb = np.uint8(img_rgb)
        template = np.uint8(template)
        #print("images converted")
        # Store width and heigth of template in w and h 
        w, h = template.shape[::-1] 
        #print("got width and height of template")
        # Perform match operations. 
        #print("This is image: {}".format(sys.argv[1]))
        a = get_match_confidence(img_gray, template)
        #print(a)
        #
        with open('threshold_lh.txt', 'r') as f:
            for line in f:
                for s in line.split(' '):
                    threshold_lh = float(s)
        #
    #
        with open('threshold_rh.txt', 'r') as f:
            for line in f:
                for s in line.split(' '):
                    threshold_rh = float(s)
     #   
    #
        #threshold_lh = 0.74
        #threshold_rh = 0.74
        count = 0
        if hand == "LH":
            threshold = threshold_lh
        elif hand == "RH":
            threshold = threshold_rh
        #print("Image {} has a confidence level of {}".format(sys.argv[1], a) + " on the {}".format(hand) + str(count) + " " + pic + " ")        
        if float(a) >= threshold:
            count += 1
            print("Detected Image {} has a confidence level of {}".format(sys.argv[1], a) + " on the {}".format(hand) + str(count) + " " + str(threshold) + " " + str(pic) + " " + str(datetime.now()))
            e.write("{},{}".format(sys.argv[1], a) + ",{}".format(hand) + str(count) + "," + str(threshold) + "," + str(pic) + "," + str(datetime.now()))
            if hand == "LH":
                self.LH += 1
                e.write("DETECTED,IMAGE,LEFT,HAND,{}\n".format(sys.argv[1]))
            elif hand == "RH":
                self.RH += 1
                e.write("DETECTED,IMAGE,RIGHT,HAND,{}\n".format(sys.argv[1]))
            else:
                error()
                sys.exit(1)
#define a class for the image processor:
#set it to a random port on the cpu
#my i3 is prob. dead
X = checkImage()
LH_ = 0
RH_ = 0
for x in ['LH', 'RH']:
    X.checkHandImage(x)
    #if x == "LH":
    #elif x == "RH":
        
    #else:
     #   error()
        


