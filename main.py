import sys
import cv2
import time
import os



def file_len(fname):
    i = 0
    with open(fname) as f:
        for i, l in enumerate(f):
            pass
    return i + 1



print("main.py <file.mp4> <thresholdRH> <thresholdLH> <templateLH> <templateRH> <fileout>")

with open('threshold_rh.txt', 'w') as f:
    x = str(sys.argv[2])
    x = x.rstrip()
    f.write(x)


with open('threshold_lh.txt', 'w') as f:
    x = str(sys.argv[3])
    x = x.rstrip()
    f.write(x)


def video_to_frames(input_loc, output_loc):
    """Function to extract frames from input video file
    and save them as separate frames in an output directory.
    Args:
        input_loc: Input video file.
        output_loc: Output directory to save the frames.
    Returns:
        None
    """
    try:
        os.mkdir(output_loc)
    except OSError:
        pass
    # Log the time
    time_start = time.time()
    # Start capturing the feed
    cap = cv2.VideoCapture(input_loc)
    # Find the number of frames
    video_length = int(cap.get(cv2.CAP_PROP_FRAME_COUNT)) - 1
    print ("Number of frames: ", video_length)
    count = 0
    print ("Converting video..\n")
    # Start converting the video
    while cap.isOpened():
        # Extract the frame
        ret, frame = cap.read()
        # Write the results back to output location.
        sys.stdout.write("\rExporting frame " + str(count) + " of " + str(video_length) + " " + "{:.0%}".format(count/video_length) + " complete.")
        sys.stdout.flush()
        #print("Exporting frame " + str(count) + " of " + str(video_length))
        #print("{:.0%}".format(count/video_length) + " complete.")
        cv2.imwrite(output_loc + "/%#05d.jpg" % (count+1), frame)
        count = count + 1
        # If there are no more frames left
        if (count > (video_length-1)):
            # Log the time again
            time_end = time.time()
            # Release the feed
            cap.release()
            # Print stats
            print ("Done extracting frames.\n%d frames extracted" % count)
            print ("It took %d seconds forconversion." % (time_end-time_start))
            break


video_to_frames(sys.argv[1], 'fole_frames')

counter = 0
for filename in os.listdir('fole_frames'):
    if filename.endswith(".jpg") or filename.endswith(".py"):
        #print('image found. now reading.')
        img_rgb = cv2.imread(os.path.join('fole_frames', filename))
        #print(filename)
        #print(os.path.join('fole_frames', filename))
        #print(img_rgb)
        cmd = 'py -3 tmoiw_bh.py {} {} {} {}'.format(os.path.join('fole_frames', filename), sys.argv[4], sys.argv[5], sys.argv[6])
        #run
        os.system(cmd)
        #pause()
        
        ab = (counter / len(os.listdir('fole_frames'))) * 100
        counter = counter + 1
        #lh_nol = file_len('LH.txt')
        #rh_nol = file_len('LH.txt')
        sys.stdout.write("Checking image {} {}% completed.".format(filename, ab) + '\n')
        sys.stdout.flush()
    else:
        continue
    
