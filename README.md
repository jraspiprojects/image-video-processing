A little piece of code that finds certain images within the video

FAQ:

    What does this program do?
    
        --> Scans a video for matching images

                    
        
        
        
[PULL REQUEST FOR LICENSE](https://github.com/jraspiprojects/image-video-processing/pull/1)


How to run:
        main.py <file.mp4> <thresholdRH> <thresholdLH> <templateLH> <templateRH> <fileout>
    
file.mp4 == video
thresholdRH == the threshold for the first image
thresholdLH == the threshold for the second image
templateLH == the image to compare with thresholdLH
templateRH == the image to compare with thresholdRH
fileout == the file that exports log data


P.S i took part of the code for tmiow_bh.py, so if you can find that somewhere else, then that code is under some other LICENSE that i really don't know. 
